
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_21:01PM

See merge request itentialopensource/adapters/adapter-tufin_securechange!15

---

## 0.5.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-tufin_securechange!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:23PM

See merge request itentialopensource/adapters/adapter-tufin_securechange!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_21:04PM

See merge request itentialopensource/adapters/adapter-tufin_securechange!11

---

## 0.5.0 [06-05-2024]

* Updated Secure Change

See merge request itentialopensource/adapters/adapter-tufin_securechange!10

---

## 0.4.0 [05-16-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!9

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:50PM

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!8

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!7

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:19PM

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!6

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:55AM

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!5

---

## 0.3.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-tufin_securechange!4

---
