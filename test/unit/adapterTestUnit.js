/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-tufin_securechange',
      type: 'TufinSecureChange',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const TufinSecureChange = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] TufinSecureChange Adapter Test', () => {
  describe('TufinSecureChange Class Tests', () => {
    const a = new TufinSecureChange(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('tufin_securechange'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('tufin_securechange'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('TufinSecureChange', pronghornDotJson.export);
          assert.equal('TufinSecureChange', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-tufin_securechange', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('tufin_securechange'));
          assert.equal('TufinSecureChange', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-tufin_securechange', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-tufin_securechange', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#retrieveAnExistingApplicationAccessRequestByID - errors', () => {
      it('should have a retrieveAnExistingApplicationAccessRequestByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingApplicationAccessRequestByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingApplicationAccessRequestByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationAccessRequestByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.retrieveAnExistingApplicationAccessRequestByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationAccessRequestByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnApplicationAccessRequest - errors', () => {
      it('should have a updateAnApplicationAccessRequest function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnApplicationAccessRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnApplicationAccessRequest(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateAnApplicationAccessRequest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.updateAnApplicationAccessRequest('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingApplicationAccessRequests - errors', () => {
      it('should have a retrieveExistingApplicationAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingApplicationAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingApplicationAccessRequests(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingApplicationAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationAccessRequests - errors', () => {
      it('should have a updateApplicationAccessRequests function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationAccessRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApplicationAccessRequests(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateApplicationAccessRequests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationAccessRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationAccessRequest - errors', () => {
      it('should have a createApplicationAccessRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationAccessRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationAccessRequest(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createApplicationAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createApplicationAccessRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createApplicationAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnectionsWithExtendedResourcesInformation - errors', () => {
      it('should have a retrieveExistingConnectionsWithExtendedResourcesInformation function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnectionsWithExtendedResourcesInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnectionsWithExtendedResourcesInformation(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnectionsWithExtendedResourcesInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createATicketToRepairAConnection - errors', () => {
      it('should have a createATicketToRepairAConnection function', (done) => {
        try {
          assert.equal(true, typeof a.createATicketToRepairAConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createATicketToRepairAConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createATicketToRepairAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createATicketToRepairAConnection('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createATicketToRepairAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.createATicketToRepairAConnection('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createATicketToRepairAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnections - errors', () => {
      it('should have a retrieveExistingConnections function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnections(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConnections - errors', () => {
      it('should have a createConnections function', (done) => {
        try {
          assert.equal(true, typeof a.createConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createConnections(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createConnections('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConnections - errors', () => {
      it('should have a updateConnections function', (done) => {
        try {
          assert.equal(true, typeof a.updateConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConnections(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateConnections('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingConnectionByID - errors', () => {
      it('should have a retrieveAnExistingConnectionByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingConnectionByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingConnectionByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.retrieveAnExistingConnectionByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAConnection - errors', () => {
      it('should have a updateAConnection function', (done) => {
        try {
          assert.equal(true, typeof a.updateAConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateAConnection('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.updateAConnection('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnection - errors', () => {
      it('should have a deleteConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteConnection(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionId', (done) => {
        try {
          a.deleteConnection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesApplicationIdentities - errors', () => {
      it('should have a fetchesApplicationIdentities function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesApplicationIdentities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingInterfaceConnections - errors', () => {
      it('should have a retrieveExistingInterfaceConnections function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingInterfaceConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingInterfaceConnections(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingInterfaceConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.retrieveExistingInterfaceConnections('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingInterfaceConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInterfaceConnections - errors', () => {
      it('should have a createInterfaceConnections function', (done) => {
        try {
          assert.equal(true, typeof a.createInterfaceConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createInterfaceConnections(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createInterfaceConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createInterfaceConnections('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createInterfaceConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.createInterfaceConnections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createInterfaceConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingInterfaceConnectionByID - errors', () => {
      it('should have a retrieveAnExistingInterfaceConnectionByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingInterfaceConnectionByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingInterfaceConnectionByID(null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingInterfaceConnectionByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.retrieveAnExistingInterfaceConnectionByID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingInterfaceConnectionByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionInterfaceId', (done) => {
        try {
          a.retrieveAnExistingInterfaceConnectionByID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingInterfaceConnectionByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInterfaceConnection - errors', () => {
      it('should have a deleteInterfaceConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInterfaceConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteInterfaceConnection(null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.deleteInterfaceConnection('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionInterfaceId', (done) => {
        try {
          a.deleteInterfaceConnection('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnInterfaceConnection - errors', () => {
      it('should have a updateAnInterfaceConnection function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnInterfaceConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnInterfaceConnection(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateAnInterfaceConnection('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.updateAnInterfaceConnection('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionInterfaceId', (done) => {
        try {
          a.updateAnInterfaceConnection('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnInterfaceConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAConnectionToApplication - errors', () => {
      it('should have a updateAConnectionToApplication function', (done) => {
        try {
          assert.equal(true, typeof a.updateAConnectionToApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAConnectionToApplication(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateAConnectionToApplication('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationId', (done) => {
        try {
          a.updateAConnectionToApplication('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingConnectionToApplicationByID - errors', () => {
      it('should have a retrieveAnExistingConnectionToApplicationByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingConnectionToApplicationByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingConnectionToApplicationByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionToApplicationByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationId', (done) => {
        try {
          a.retrieveAnExistingConnectionToApplicationByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionToApplicationByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnectionToApplication - errors', () => {
      it('should have a deleteConnectionToApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteConnectionToApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteConnectionToApplication(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationId', (done) => {
        try {
          a.deleteConnectionToApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createApplicationInterface - errors', () => {
      it('should have a createApplicationInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createApplicationInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createApplicationInterface(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createApplicationInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingApplicationInterfaces - errors', () => {
      it('should have a retrieveExistingApplicationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingApplicationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingApplicationInterfaces(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingApplicationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationInterface - errors', () => {
      it('should have a deleteApplicationInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteApplicationInterface(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.deleteApplicationInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnApplicationInterface - errors', () => {
      it('should have a updateAnApplicationInterface function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnApplicationInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnApplicationInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateAnApplicationInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.updateAnApplicationInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingApplicationInterfaceByID - errors', () => {
      it('should have a retrieveAnExistingApplicationInterfaceByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingApplicationInterfaceByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingApplicationInterfaceByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationInterfaceByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.retrieveAnExistingApplicationInterfaceByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationInterfaceByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnectionsToApplication - errors', () => {
      it('should have a retrieveExistingConnectionsToApplication function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnectionsToApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnectionsToApplication(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnectionsToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConnectionToApplication - errors', () => {
      it('should have a createConnectionToApplication function', (done) => {
        try {
          assert.equal(true, typeof a.createConnectionToApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createConnectionToApplication(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createConnectionToApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createConnectionToApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateConnectionToApplicationMappingsForSourceAndTargetApplications - errors', () => {
      it('should have a updateConnectionToApplicationMappingsForSourceAndTargetApplications function', (done) => {
        try {
          assert.equal(true, typeof a.updateConnectionToApplicationMappingsForSourceAndTargetApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateConnectionToApplicationMappingsForSourceAndTargetApplications(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateConnectionToApplicationMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.updateConnectionToApplicationMappingsForSourceAndTargetApplications('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateConnectionToApplicationMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.updateConnectionToApplicationMappingsForSourceAndTargetApplications('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateConnectionToApplicationMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveConnectionToApplicationMappingsForSourceAndTargetApplications - errors', () => {
      it('should have a retrieveConnectionToApplicationMappingsForSourceAndTargetApplications function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveConnectionToApplicationMappingsForSourceAndTargetApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.retrieveConnectionToApplicationMappingsForSourceAndTargetApplications(null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveConnectionToApplicationMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.retrieveConnectionToApplicationMappingsForSourceAndTargetApplications('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveConnectionToApplicationMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#copyApplicationServersAndConnectionsToAnotherApplication - errors', () => {
      it('should have a copyApplicationServersAndConnectionsToAnotherApplication function', (done) => {
        try {
          assert.equal(true, typeof a.copyApplicationServersAndConnectionsToAnotherApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.copyApplicationServersAndConnectionsToAnotherApplication(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-copyApplicationServersAndConnectionsToAnotherApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.copyApplicationServersAndConnectionsToAnotherApplication('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-copyApplicationServersAndConnectionsToAnotherApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.copyApplicationServersAndConnectionsToAnotherApplication('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-copyApplicationServersAndConnectionsToAnotherApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServerMappingsForSourceAndTargetApplications - errors', () => {
      it('should have a updateServerMappingsForSourceAndTargetApplications function', (done) => {
        try {
          assert.equal(true, typeof a.updateServerMappingsForSourceAndTargetApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateServerMappingsForSourceAndTargetApplications(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServerMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.updateServerMappingsForSourceAndTargetApplications('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServerMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.updateServerMappingsForSourceAndTargetApplications('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServerMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveServerMappingsForSourceAndTargetApplications - errors', () => {
      it('should have a retrieveServerMappingsForSourceAndTargetApplications function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveServerMappingsForSourceAndTargetApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.retrieveServerMappingsForSourceAndTargetApplications(null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveServerMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.retrieveServerMappingsForSourceAndTargetApplications('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveServerMappingsForSourceAndTargetApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnApplicationPack - errors', () => {
      it('should have a updateAnApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnApplicationPack(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.updateAnApplicationPack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.updateAnApplicationPack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplicationPack - errors', () => {
      it('should have a deleteApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deleteApplicationPack(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.deleteApplicationPack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingApplicationPackByID - errors', () => {
      it('should have a retrieveAnExistingApplicationPackByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingApplicationPackByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.retrieveAnExistingApplicationPackByID(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationPackByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.retrieveAnExistingApplicationPackByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationPackByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationInterfacesAndTagMapping - errors', () => {
      it('should have a updateApplicationInterfacesAndTagMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationInterfacesAndTagMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApplicationInterfacesAndTagMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationInterfacesAndTagMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.updateApplicationInterfacesAndTagMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationInterfacesAndTagMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.updateApplicationInterfacesAndTagMapping('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationInterfacesAndTagMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationInterfacesFromApplicationPack - errors', () => {
      it('should have a removeApplicationInterfacesFromApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationInterfacesFromApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.removeApplicationInterfacesFromApplicationPack(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-removeApplicationInterfacesFromApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.removeApplicationInterfacesFromApplicationPack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-removeApplicationInterfacesFromApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllApplicationPackTags - errors', () => {
      it('should have a listAllApplicationPackTags function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApplicationPackTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listAllApplicationPackTags(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-listAllApplicationPackTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.listAllApplicationPackTags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-listAllApplicationPackTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTagsOfAnApplicationPack - errors', () => {
      it('should have a modifyTagsOfAnApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTagsOfAnApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyTagsOfAnApplicationPack(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTagsOfAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.modifyTagsOfAnApplicationPack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTagsOfAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.modifyTagsOfAnApplicationPack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTagsOfAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewTagUnderAnApplicationPack - errors', () => {
      it('should have a createNewTagUnderAnApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.createNewTagUnderAnApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNewTagUnderAnApplicationPack(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewTagUnderAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.createNewTagUnderAnApplicationPack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewTagUnderAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.createNewTagUnderAnApplicationPack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewTagUnderAnApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTagsFromApplicationPack - errors', () => {
      it('should have a deleteTagsFromApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTagsFromApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deleteTagsFromApplicationPack(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteTagsFromApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.deleteTagsFromApplicationPack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteTagsFromApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAConnectionToApplicationPack - errors', () => {
      it('should have a deleteAConnectionToApplicationPack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAConnectionToApplicationPack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteAConnectionToApplicationPack(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteAConnectionToApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationPackId', (done) => {
        try {
          a.deleteAConnectionToApplicationPack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteAConnectionToApplicationPack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingConnectionToApplicationPackByID - errors', () => {
      it('should have a retrieveAnExistingConnectionToApplicationPackByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingConnectionToApplicationPackByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingConnectionToApplicationPackByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionToApplicationPackByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationPackId', (done) => {
        try {
          a.retrieveAnExistingConnectionToApplicationPackByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingConnectionToApplicationPackByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId - errors', () => {
      it('should have a putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId function', (done) => {
        try {
          assert.equal(true, typeof a.putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionToApplicationPackId', (done) => {
        try {
          a.putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionToApplicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-putSecurechangeworkflowApiSecureappRepositoryApplicationsApplicationIdConnectionToApplicationPacksConnectionToApplicationPackId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplicationPacks - errors', () => {
      it('should have a updateApplicationPacks function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplicationPacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApplicationPacks(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.updateApplicationPacks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllApplicationPacks - errors', () => {
      it('should have a listAllApplicationPacks function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApplicationPacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listAllApplicationPacks(null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-listAllApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewApplicationPacks - errors', () => {
      it('should have a createNewApplicationPacks function', (done) => {
        try {
          assert.equal(true, typeof a.createNewApplicationPacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNewApplicationPacks(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.createNewApplicationPacks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagByID - errors', () => {
      it('should have a getTagByID function', (done) => {
        try {
          assert.equal(true, typeof a.getTagByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getTagByID(null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getTagByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.getTagByID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getTagByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tagId', (done) => {
        try {
          a.getTagByID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tagId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getTagByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewConnectionToApplicationPacks - errors', () => {
      it('should have a createNewConnectionToApplicationPacks function', (done) => {
        try {
          assert.equal(true, typeof a.createNewConnectionToApplicationPacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNewConnectionToApplicationPacks(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewConnectionToApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createNewConnectionToApplicationPacks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewConnectionToApplicationPacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAListOfAllConnectionToApplicationPackByApplicationID - errors', () => {
      it('should have a retrieveAListOfAllConnectionToApplicationPackByApplicationID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAListOfAllConnectionToApplicationPackByApplicationID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAListOfAllConnectionToApplicationPackByApplicationID(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAListOfAllConnectionToApplicationPackByApplicationID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingAccessRules - errors', () => {
      it('should have a retrieveExistingAccessRules function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingAccessRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingAccessRules(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingAccessRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServers - errors', () => {
      it('should have a fetchesServers function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServerByID - errors', () => {
      it('should have a fetchesServerByID function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServerByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.fetchesServerByID(null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-fetchesServerByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingServers - errors', () => {
      it('should have a retrieveExistingServers function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingServers(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServers - errors', () => {
      it('should have a updateServers function', (done) => {
        try {
          assert.equal(true, typeof a.updateServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateServers(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateServers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServers - errors', () => {
      it('should have a createServers function', (done) => {
        try {
          assert.equal(true, typeof a.createServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createServers(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createServers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServer - errors', () => {
      it('should have a deleteServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteServer(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.deleteServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingServerByID - errors', () => {
      it('should have a retrieveAnExistingServerByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingServerByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingServerByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingServerByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.retrieveAnExistingServerByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingServerByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine - errors', () => {
      it('should have a ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine function', (done) => {
        try {
          assert.equal(true, typeof a.ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-ifTheIdentifiedServerIsAGroupReturnAListOfItsMembersWhenTheMediaTypeIsTextPlainDefaultPrintEachMemberOnASeparateLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveServer - errors', () => {
      it('should have a moveServer function', (done) => {
        try {
          assert.equal(true, typeof a.moveServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.moveServer(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-moveServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.moveServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-moveServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportServerImpactAnalysisToACSVFile - errors', () => {
      it('should have a exportServerImpactAnalysisToACSVFile function', (done) => {
        try {
          assert.equal(true, typeof a.exportServerImpactAnalysisToACSVFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.exportServerImpactAnalysisToACSVFile(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-exportServerImpactAnalysisToACSVFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.exportServerImpactAnalysisToACSVFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-exportServerImpactAnalysisToACSVFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyUnusedAndNonPredefinedGlobalServices - errors', () => {
      it('should have a modifyUnusedAndNonPredefinedGlobalServices function', (done) => {
        try {
          assert.equal(true, typeof a.modifyUnusedAndNonPredefinedGlobalServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyUnusedAndNonPredefinedGlobalServices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyUnusedAndNonPredefinedGlobalServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnusedAndNonPredefinedGlobalServiceByName - errors', () => {
      it('should have a deleteUnusedAndNonPredefinedGlobalServiceByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnusedAndNonPredefinedGlobalServiceByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServices - errors', () => {
      it('should have a fetchesServices function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGlobalServices - errors', () => {
      it('should have a createGlobalServices function', (done) => {
        try {
          assert.equal(true, typeof a.createGlobalServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createGlobalServices(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createGlobalServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServiceByID - errors', () => {
      it('should have a fetchesServiceByID function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServiceByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.fetchesServiceByID(null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-fetchesServiceByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnusedAndNonPredefinedGlobalService - errors', () => {
      it('should have a deleteUnusedAndNonPredefinedGlobalService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnusedAndNonPredefinedGlobalService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteUnusedAndNonPredefinedGlobalService(null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteUnusedAndNonPredefinedGlobalService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingServices - errors', () => {
      it('should have a retrieveExistingServices function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingServices(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServices - errors', () => {
      it('should have a createServices function', (done) => {
        try {
          assert.equal(true, typeof a.createServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createServices(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.createServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateServices - errors', () => {
      it('should have a updateServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateServices(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.updateServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteService - errors', () => {
      it('should have a deleteService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteService(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingServiceByID - errors', () => {
      it('should have a retrieveAnExistingServiceByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingServiceByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingServiceByID(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingServiceByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.retrieveAnExistingServiceByID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingServiceByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesUsers - errors', () => {
      it('should have a fetchesUsers function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUsers - errors', () => {
      it('should have a createUsers function', (done) => {
        try {
          assert.equal(true, typeof a.createUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesUserByID - errors', () => {
      it('should have a fetchesUserByID function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesUserByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.fetchesUserByID(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-fetchesUserByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnusedUser - errors', () => {
      it('should have a deleteUnusedUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnusedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUnusedUser(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteUnusedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveSecurityPolicyViolationsFromSecureTrack - errors', () => {
      it('should have a retrieveSecurityPolicyViolationsFromSecureTrack function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveSecurityPolicyViolationsFromSecureTrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveSecurityPolicyViolationsFromSecureTrack(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveSecurityPolicyViolationsFromSecureTrack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllApplications - errors', () => {
      it('should have a listAllApplications function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNewApplications - errors', () => {
      it('should have a createNewApplications function', (done) => {
        try {
          assert.equal(true, typeof a.createNewApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNewApplications(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createNewApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateApplications - errors', () => {
      it('should have a updateApplications function', (done) => {
        try {
          assert.equal(true, typeof a.updateApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateApplications(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateApplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnApplication - errors', () => {
      it('should have a deleteAnApplication function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteAnApplication(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteAnApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAnExistingApplicationByID - errors', () => {
      it('should have a retrieveAnExistingApplicationByID function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAnExistingApplicationByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAnExistingApplicationByID(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAnExistingApplicationByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeAnApplication - errors', () => {
      it('should have a changeAnApplication function', (done) => {
        try {
          assert.equal(true, typeof a.changeAnApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeAnApplication(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAnApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.changeAnApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAnApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportApplicationHistoryToPDF - errors', () => {
      it('should have a exportApplicationHistoryToPDF function', (done) => {
        try {
          assert.equal(true, typeof a.exportApplicationHistoryToPDF === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.exportApplicationHistoryToPDF(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-exportApplicationHistoryToPDF', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveApplicationsToAnotherCustomer - errors', () => {
      it('should have a moveApplicationsToAnotherCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.moveApplicationsToAnotherCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.moveApplicationsToAnotherCustomer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-moveApplicationsToAnotherCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveApplicationHistory - errors', () => {
      it('should have a retrieveApplicationHistory function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveApplicationHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveApplicationHistory(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveApplicationHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportImpactAnalysisToACSVFile - errors', () => {
      it('should have a exportImpactAnalysisToACSVFile function', (done) => {
        try {
          assert.equal(true, typeof a.exportImpactAnalysisToACSVFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.exportImpactAnalysisToACSVFile(null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-exportImpactAnalysisToACSVFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#autoAssociateCloudServersFromSecureTrackWithAnApplication - errors', () => {
      it('should have a autoAssociateCloudServersFromSecureTrackWithAnApplication function', (done) => {
        try {
          assert.equal(true, typeof a.autoAssociateCloudServersFromSecureTrackWithAnApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.autoAssociateCloudServersFromSecureTrackWithAnApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-autoAssociateCloudServersFromSecureTrackWithAnApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveCloudServerListingFromSecureTrack - errors', () => {
      it('should have a retrieveCloudServerListingFromSecureTrack function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveCloudServerListingFromSecureTrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateCloudServersFromSecureTrackWithAnApplication - errors', () => {
      it('should have a associateCloudServersFromSecureTrackWithAnApplication function', (done) => {
        try {
          assert.equal(true, typeof a.associateCloudServersFromSecureTrackWithAnApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.associateCloudServersFromSecureTrackWithAnApplication(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-associateCloudServersFromSecureTrackWithAnApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importCustomers - errors', () => {
      it('should have a importCustomers function', (done) => {
        try {
          assert.equal(true, typeof a.importCustomers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importCustomers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-importCustomers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllCustomers - errors', () => {
      it('should have a listAllCustomers function', (done) => {
        try {
          assert.equal(true, typeof a.listAllCustomers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingApplicationsForSpecificDomain - errors', () => {
      it('should have a retrieveExistingApplicationsForSpecificDomain function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingApplicationsForSpecificDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.retrieveExistingApplicationsForSpecificDomain(null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingApplicationsForSpecificDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllImportedCustomers - errors', () => {
      it('should have a listAllImportedCustomers function', (done) => {
        try {
          assert.equal(true, typeof a.listAllImportedCustomers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesCustomerByID - errors', () => {
      it('should have a fetchesCustomerByID function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesCustomerByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.fetchesCustomerByID(null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-fetchesCustomerByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateACustomer - errors', () => {
      it('should have a updateACustomer function', (done) => {
        try {
          assert.equal(true, typeof a.updateACustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateACustomer(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateACustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.updateACustomer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateACustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllNonImportedCustomers - errors', () => {
      it('should have a listAllNonImportedCustomers function', (done) => {
        try {
          assert.equal(true, typeof a.listAllNonImportedCustomers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#synchronizeDomains - errors', () => {
      it('should have a synchronizeDomains function', (done) => {
        try {
          assert.equal(true, typeof a.synchronizeDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.synchronizeDomains(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-synchronizeDomains', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllDomains - errors', () => {
      it('should have a listAllDomains function', (done) => {
        try {
          assert.equal(true, typeof a.listAllDomains === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesDomainByID - errors', () => {
      it('should have a fetchesDomainByID function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesDomainByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainId', (done) => {
        try {
          a.fetchesDomainByID(null, (data, error) => {
            try {
              const displayE = 'domainId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-fetchesDomainByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesVirtualServersFromSecureTrack - errors', () => {
      it('should have a fetchesVirtualServersFromSecureTrack function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesVirtualServersFromSecureTrack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChange - errors', () => {
      it('should have a returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChange function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuggestedTargetsForAGivenAccessRequest - errors', () => {
      it('should have a getSuggestedTargetsForAGivenAccessRequest function', (done) => {
        try {
          assert.equal(true, typeof a.getSuggestedTargetsForAGivenAccessRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.getSuggestedTargetsForAGivenAccessRequest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSuggestedTargetsForAGivenAccessRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheWholeListOfExcludedDevicesInSecureChange - errors', () => {
      it('should have a updateTheWholeListOfExcludedDevicesInSecureChange function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheWholeListOfExcludedDevicesInSecureChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheWholeListOfExcludedDevicesInSecureChange(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updateTheWholeListOfExcludedDevicesInSecureChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChange - errors', () => {
      it('should have a returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChange function', (done) => {
        try {
          assert.equal(true, typeof a.returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runServerDecommissionDesignerForGivenTask - errors', () => {
      it('should have a runServerDecommissionDesignerForGivenTask function', (done) => {
        try {
          assert.equal(true, typeof a.runServerDecommissionDesignerForGivenTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runServerDecommissionDesignerForGivenTask(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runServerDecommissionDesignerForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.runServerDecommissionDesignerForGivenTask('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runServerDecommissionDesignerForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.runServerDecommissionDesignerForGivenTask('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runServerDecommissionDesignerForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runImpactAnalysisForGivenTask - errors', () => {
      it('should have a runImpactAnalysisForGivenTask function', (done) => {
        try {
          assert.equal(true, typeof a.runImpactAnalysisForGivenTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runImpactAnalysisForGivenTask(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runImpactAnalysisForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.runImpactAnalysisForGivenTask('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runImpactAnalysisForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.runImpactAnalysisForGivenTask('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runImpactAnalysisForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImpactAnalysisResults - errors', () => {
      it('should have a getImpactAnalysisResults function', (done) => {
        try {
          assert.equal(true, typeof a.getImpactAnalysisResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getImpactAnalysisResults(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getImpactAnalysisResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getImpactAnalysisResults('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getImpactAnalysisResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getImpactAnalysisResults('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getImpactAnalysisResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getImpactAnalysisResults('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getImpactAnalysisResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runVerifierForGivenTask - errors', () => {
      it('should have a runVerifierForGivenTask function', (done) => {
        try {
          assert.equal(true, typeof a.runVerifierForGivenTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.runVerifierForGivenTask(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runVerifierForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.runVerifierForGivenTask('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runVerifierForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.runVerifierForGivenTask('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runVerifierForGivenTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVerifierResults - errors', () => {
      it('should have a getVerifierResults function', (done) => {
        try {
          assert.equal(true, typeof a.getVerifierResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVerifierResults(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getVerifierResults('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getVerifierResults('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getVerifierResults('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerDecommissionDesignerResults - errors', () => {
      it('should have a getServerDecommissionDesignerResults function', (done) => {
        try {
          assert.equal(true, typeof a.getServerDecommissionDesignerResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getServerDecommissionDesignerResults(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerDecommissionDesignerResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getServerDecommissionDesignerResults('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerDecommissionDesignerResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getServerDecommissionDesignerResults('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerDecommissionDesignerResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getServerDecommissionDesignerResults('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerDecommissionDesignerResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus - errors', () => {
      it('should have a modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveTheSecureTrackConnectionSettingsAndStatus - errors', () => {
      it('should have a retrieveTheSecureTrackConnectionSettingsAndStatus function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveTheSecureTrackConnectionSettingsAndStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importUserFromLDAP - errors', () => {
      it('should have a importUserFromLDAP function', (done) => {
        try {
          assert.equal(true, typeof a.importUserFromLDAP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importUserFromLDAP(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-importUserFromLDAP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheListOfUsers - errors', () => {
      it('should have a returnTheListOfUsers function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheListOfUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSpecifiedUser - errors', () => {
      it('should have a returnTheSpecifiedUser function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSpecifiedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnTheSpecifiedUser(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectATicket - errors', () => {
      it('should have a rejectATicket function', (done) => {
        try {
          assert.equal(true, typeof a.rejectATicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rejectATicket(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-rejectATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rejectATicket('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-rejectATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mapRulesToTicket - errors', () => {
      it('should have a mapRulesToTicket function', (done) => {
        try {
          assert.equal(true, typeof a.mapRulesToTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.mapRulesToTicket(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-mapRulesToTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.mapRulesToTicket('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-mapRulesToTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reassignsTheSpecifiedTicketTaskToTheSpecifiedUser - errors', () => {
      it('should have a reassignsTheSpecifiedTicketTaskToTheSpecifiedUser function', (done) => {
        try {
          assert.equal(true, typeof a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-reassignsTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-reassignsTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-reassignsTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-reassignsTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assigneeId', (done) => {
        try {
          a.reassignsTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'assigneeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-reassignsTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmTheRequest - errors', () => {
      it('should have a confirmTheRequest function', (done) => {
        try {
          assert.equal(true, typeof a.confirmTheRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.confirmTheRequest(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-confirmTheRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.confirmTheRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-confirmTheRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser - errors', () => {
      it('should have a changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser function', (done) => {
        try {
          assert.equal(true, typeof a.changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assigneeId', (done) => {
        try {
          a.changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'assigneeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeTheRequesterOfTheSpecifiedTicketTaskToTheSpecifiedUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheSpecifiedTicketToTheSpecifiedStep - errors', () => {
      it('should have a returnsTheSpecifiedTicketToTheSpecifiedStep function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheSpecifiedTicketToTheSpecifiedStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.returnsTheSpecifiedTicketToTheSpecifiedStep(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheSpecifiedTicketToTheSpecifiedStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheSpecifiedTicketToTheSpecifiedStep('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheSpecifiedTicketToTheSpecifiedStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheSpecifiedTicketToTheSpecifiedStep('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheSpecifiedTicketToTheSpecifiedStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheSpecifiedTicketToTheSpecifiedStep('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheSpecifiedTicketToTheSpecifiedStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toStepId', (done) => {
        try {
          a.returnsTheSpecifiedTicketToTheSpecifiedStep('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'toStepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheSpecifiedTicketToTheSpecifiedStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheHistoryOfTheSpecifiedTicket - errors', () => {
      it('should have a returnsTheHistoryOfTheSpecifiedTicket function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheHistoryOfTheSpecifiedTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheHistoryOfTheSpecifiedTicket(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheHistoryOfTheSpecifiedTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelATicket - errors', () => {
      it('should have a cancelATicket function', (done) => {
        try {
          assert.equal(true, typeof a.cancelATicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.cancelATicket(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-cancelATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.cancelATicket('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-cancelATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfATheFields - errors', () => {
      it('should have a returnsTheDetailsOfATheFields function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfATheFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfATheFields(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfATheFields('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfATheFields('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeMultipleFieldsWithinATaskOfATicket - errors', () => {
      it('should have a changeMultipleFieldsWithinATaskOfATicket function', (done) => {
        try {
          assert.equal(true, typeof a.changeMultipleFieldsWithinATaskOfATicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeMultipleFieldsWithinATaskOfATicket(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldsWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeMultipleFieldsWithinATaskOfATicket('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldsWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.changeMultipleFieldsWithinATaskOfATicket('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldsWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeMultipleFieldsWithinATaskOfATicket('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldsWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommandsForDeviceFromDesignerResultsOfASpecificFieldId - errors', () => {
      it('should have a getCommandsForDeviceFromDesignerResultsOfASpecificFieldId function', (done) => {
        try {
          assert.equal(true, typeof a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getCommandsForDeviceFromDesignerResultsOfASpecificFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getCommandsForDeviceFromDesignerResultsOfASpecificFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getCommandsForDeviceFromDesignerResultsOfASpecificFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getCommandsForDeviceFromDesignerResultsOfASpecificFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.getCommandsForDeviceFromDesignerResultsOfASpecificFieldId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getCommandsForDeviceFromDesignerResultsOfASpecificFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeTheTicketProperties - errors', () => {
      it('should have a changeTheTicketProperties function', (done) => {
        try {
          assert.equal(true, typeof a.changeTheTicketProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeTheTicketProperties(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeTheTicketProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeTheTicketProperties('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeTheTicketProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeATaskOfATicketAtTheCurrentStep - errors', () => {
      it('should have a changeATaskOfATicketAtTheCurrentStep function', (done) => {
        try {
          assert.equal(true, typeof a.changeATaskOfATicketAtTheCurrentStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeATaskOfATicketAtTheCurrentStep(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeATaskOfATicketAtTheCurrentStep('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeATaskOfATicketAtTheCurrentStep('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedField - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedField function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedField === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedField(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedField('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedField('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedField('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedField', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeAFieldWithinATaskOfATicket - errors', () => {
      it('should have a changeAFieldWithinATaskOfATicket function', (done) => {
        try {
          assert.equal(true, typeof a.changeAFieldWithinATaskOfATicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicket(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicket('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicket('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicket('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicket('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRuleDecommisionVerifierResults - errors', () => {
      it('should have a getRuleDecommisionVerifierResults function', (done) => {
        try {
          assert.equal(true, typeof a.getRuleDecommisionVerifierResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRuleDecommisionVerifierResults(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRuleDecommisionVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getRuleDecommisionVerifierResults('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRuleDecommisionVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getRuleDecommisionVerifierResults('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRuleDecommisionVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDesignerResultsAccordingToFieldId - errors', () => {
      it('should have a getDesignerResultsAccordingToFieldId function', (done) => {
        try {
          assert.equal(true, typeof a.getDesignerResultsAccordingToFieldId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDesignerResultsAccordingToFieldId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsAccordingToFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getDesignerResultsAccordingToFieldId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsAccordingToFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getDesignerResultsAccordingToFieldId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsAccordingToFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getDesignerResultsAccordingToFieldId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsAccordingToFieldId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheResultsOfTheCommitAction - errors', () => {
      it('should have a returnsTheResultsOfTheCommitAction function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheResultsOfTheCommitAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheResultsOfTheCommitAction(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheResultsOfTheCommitAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheResultsOfTheCommitAction('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheResultsOfTheCommitAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheResultsOfTheCommitAction('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheResultsOfTheCommitAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.returnsTheResultsOfTheCommitAction('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheResultsOfTheCommitAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managementId', (done) => {
        try {
          a.returnsTheResultsOfTheCommitAction('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'managementId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheResultsOfTheCommitAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicketStages - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicketStages function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicketStages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTicketStages(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTicketStages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeAFieldWithinATaskOfATicketAtTheCurrentStep - errors', () => {
      it('should have a changeAFieldWithinATaskOfATicketAtTheCurrentStep function', (done) => {
        try {
          assert.equal(true, typeof a.changeAFieldWithinATaskOfATicketAtTheCurrentStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicketAtTheCurrentStep(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicketAtTheCurrentStep('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicketAtTheCurrentStep('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.changeAFieldWithinATaskOfATicketAtTheCurrentStep('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeAFieldWithinATaskOfATicketAtTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeMultipleFieldWithinATaskOfATicketInTheCurrentStep - errors', () => {
      it('should have a changeMultipleFieldWithinATaskOfATicketInTheCurrentStep function', (done) => {
        try {
          assert.equal(true, typeof a.changeMultipleFieldWithinATaskOfATicketInTheCurrentStep === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeMultipleFieldWithinATaskOfATicketInTheCurrentStep(null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldWithinATaskOfATicketInTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeMultipleFieldWithinATaskOfATicketInTheCurrentStep('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldWithinATaskOfATicketInTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeMultipleFieldWithinATaskOfATicketInTheCurrentStep('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeMultipleFieldWithinATaskOfATicketInTheCurrentStep', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy - errors', () => {
      it('should have a modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instructionId', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instructionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId - errors', () => {
      it('should have a getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsAListOfTicketsBySearchParameters - errors', () => {
      it('should have a returnsAListOfTicketsBySearchParameters function', (done) => {
        try {
          assert.equal(true, typeof a.returnsAListOfTicketsBySearchParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServerClonePolicyVerifierResults - errors', () => {
      it('should have a getServerClonePolicyVerifierResults function', (done) => {
        try {
          assert.equal(true, typeof a.getServerClonePolicyVerifierResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getServerClonePolicyVerifierResults(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerClonePolicyVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getServerClonePolicyVerifierResults('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerClonePolicyVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getServerClonePolicyVerifierResults('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServerClonePolicyVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicket - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicket function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTicket(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#submitANewTicketToSecureChange - errors', () => {
      it('should have a submitANewTicketToSecureChange function', (done) => {
        try {
          assert.equal(true, typeof a.submitANewTicketToSecureChange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.submitANewTicketToSecureChange(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-submitANewTicketToSecureChange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurechangeworkflowApiSecurechangeTicketsSearch - errors', () => {
      it('should have a getSecurechangeworkflowApiSecurechangeTicketsSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurechangeworkflowApiSecurechangeTicketsSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTopologyMapImagePngFileForSpecificVerifierResults - errors', () => {
      it('should have a returnsTopologyMapImagePngFileForSpecificVerifierResults function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTopologyMapImagePngFileForSpecificVerifierResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTopologyMapImagePngFileForSpecificVerifierResults(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTopologyMapImagePngFileForSpecificVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTopologyMapImagePngFileForSpecificVerifierResults('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTopologyMapImagePngFileForSpecificVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTopologyMapImagePngFileForSpecificVerifierResults('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTopologyMapImagePngFileForSpecificVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.returnsTopologyMapImagePngFileForSpecificVerifierResults('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTopologyMapImagePngFileForSpecificVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccessRequestVerifierResults - errors', () => {
      it('should have a getAccessRequestVerifierResults function', (done) => {
        try {
          assert.equal(true, typeof a.getAccessRequestVerifierResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getAccessRequestVerifierResults(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getAccessRequestVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getAccessRequestVerifierResults('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getAccessRequestVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getAccessRequestVerifierResults('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getAccessRequestVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getAccessRequestVerifierResults('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getAccessRequestVerifierResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTask - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTask function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTask(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTask('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTask('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeATaskOfATicket - errors', () => {
      it('should have a changeATaskOfATicket function', (done) => {
        try {
          assert.equal(true, typeof a.changeATaskOfATicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeATaskOfATicket(null, null, null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeATaskOfATicket('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.changeATaskOfATicket('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.changeATaskOfATicket('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeATaskOfATicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfTheTasks - errors', () => {
      it('should have a returnsTheDetailsOfTheTasks function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfTheTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfTheTasks(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfTheTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfTheTasks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfTheTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServersMapping - errors', () => {
      it('should have a getServersMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getServersMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.getServersMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServersMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.getServersMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getServersMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyServersMapping - errors', () => {
      it('should have a modifyServersMapping function', (done) => {
        try {
          assert.equal(true, typeof a.modifyServersMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.modifyServersMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyServersMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.modifyServersMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyServersMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceInstanceMapping - errors', () => {
      it('should have a getInterfaceInstanceMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceInstanceMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.getInterfaceInstanceMapping(null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getInterfaceInstanceMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.getInterfaceInstanceMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getInterfaceInstanceMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInterfaceInstanceMapping - errors', () => {
      it('should have a modifyInterfaceInstanceMapping function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInterfaceInstanceMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.modifyInterfaceInstanceMapping(null, null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyInterfaceInstanceMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.modifyInterfaceInstanceMapping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyInterfaceInstanceMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#migrateApplication - errors', () => {
      it('should have a migrateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.migrateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceApplicationId', (done) => {
        try {
          a.migrateApplication(null, null, (data, error) => {
            try {
              const displayE = 'sourceApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-migrateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetApplicationId', (done) => {
        try {
          a.migrateApplication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetApplicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-migrateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#openUploadForm - errors', () => {
      it('should have a openUploadForm function', (done) => {
        try {
          assert.equal(true, typeof a.openUploadForm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadExcel - errors', () => {
      it('should have a uploadExcel function', (done) => {
        try {
          assert.equal(true, typeof a.uploadExcel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.uploadExcel(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-uploadExcel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadEmptyExcel - errors', () => {
      it('should have a downloadEmptyExcel function', (done) => {
        try {
          assert.equal(true, typeof a.downloadEmptyExcel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllApplicationsQuery - errors', () => {
      it('should have a listAllApplicationsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApplicationsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveApplicationHistoryQuery - errors', () => {
      it('should have a retrieveApplicationHistoryQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveApplicationHistoryQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveApplicationHistoryQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveApplicationHistoryQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportApplicationHistoryToPDFQuery - errors', () => {
      it('should have a exportApplicationHistoryToPDFQuery function', (done) => {
        try {
          assert.equal(true, typeof a.exportApplicationHistoryToPDFQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.exportApplicationHistoryToPDFQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-exportApplicationHistoryToPDFQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileAttachment - errors', () => {
      it('should have a getFileAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getFileAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uid', (done) => {
        try {
          a.getFileAttachment(null, (data, error) => {
            try {
              const displayE = 'uid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getFileAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAttachment - errors', () => {
      it('should have a addAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.addAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAttachment(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-addAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runRuleRecertificationUpdateMetadata - errors', () => {
      it('should have a runRuleRecertificationUpdateMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.runRuleRecertificationUpdateMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.runRuleRecertificationUpdateMetadata(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runRuleRecertificationUpdateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.runRuleRecertificationUpdateMetadata('fakeparam', null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-runRuleRecertificationUpdateMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRelatedRules - errors', () => {
      it('should have a getRelatedRules function', (done) => {
        try {
          assert.equal(true, typeof a.getRelatedRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRelatedRules(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRelatedRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getRelatedRules('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRelatedRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getRelatedRules('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRelatedRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getRelatedRules('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRelatedRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZones - errors', () => {
      it('should have a getSecurityZones function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurityZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSecurityZones(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurityZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getSecurityZones('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurityZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getSecurityZones('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurityZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getSecurityZones('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurityZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDesignerResultsByDeviceId - errors', () => {
      it('should have a getDesignerResultsByDeviceId function', (done) => {
        try {
          assert.equal(true, typeof a.getDesignerResultsByDeviceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDesignerResultsByDeviceId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getDesignerResultsByDeviceId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getDesignerResultsByDeviceId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.getDesignerResultsByDeviceId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing managementId', (done) => {
        try {
          a.getDesignerResultsByDeviceId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'managementId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getDesignerResultsByDeviceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addComment - errors', () => {
      it('should have a addComment function', (done) => {
        try {
          assert.equal(true, typeof a.addComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ticketId', (done) => {
        try {
          a.addComment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'ticketId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-addComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.addComment('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-addComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.addComment('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-addComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addComment('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-addComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteComment - errors', () => {
      it('should have a deleteComment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ticketId', (done) => {
        try {
          a.deleteComment(null, null, (data, error) => {
            try {
              const displayE = 'ticketId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deleteComment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicketQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicketQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicketQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTicketQuery(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTicketQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurechangeworkflowApiSecurechangeTicketsSearchQuery - errors', () => {
      it('should have a getSecurechangeworkflowApiSecurechangeTicketsSearchQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurechangeworkflowApiSecurechangeTicketsSearchQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedFieldQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedFieldQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedFieldQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedFieldQuery(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedFieldQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedFieldQuery('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedFieldQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedFieldQuery('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedFieldQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fieldId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedFieldQuery('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fieldId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedFieldQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicketStagesQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicketStagesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicketStagesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTicketStagesQuery(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTicketStagesQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery - errors', () => {
      it('should have a modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instructionId', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instructionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-modifyTheDesignerSuggestionsForAccessRequestOrCloneServerPolicyQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQueryQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQueryQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTicketsMax100TicketsInASingleQueryQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#submitANewTicketToSecureChangeQuery - errors', () => {
      it('should have a submitANewTicketToSecureChangeQuery function', (done) => {
        try {
          assert.equal(true, typeof a.submitANewTicketToSecureChangeQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.submitANewTicketToSecureChangeQuery('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-submitANewTicketToSecureChangeQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery - errors', () => {
      it('should have a getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getSecurechangeworkflowApiSecurechangeTicketsIdStepsStepIdQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfTheTasksQuery - errors', () => {
      it('should have a returnsTheDetailsOfTheTasksQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfTheTasksQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfTheTasksQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfTheTasksQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfTheTasksQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfTheTasksQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfASpecifiedTaskQuery - errors', () => {
      it('should have a returnsTheDetailsOfASpecifiedTaskQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfASpecifiedTaskQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTaskQuery(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTaskQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTaskQuery('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTaskQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfASpecifiedTaskQuery('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfASpecifiedTaskQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfATheFieldsQuery - errors', () => {
      it('should have a returnsTheDetailsOfATheFieldsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfATheFieldsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsTheDetailsOfATheFieldsQuery(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFieldsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing stepId', (done) => {
        try {
          a.returnsTheDetailsOfATheFieldsQuery('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'stepId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFieldsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.returnsTheDetailsOfATheFieldsQuery('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-returnsTheDetailsOfATheFieldsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsAListOfTicketsBySearchParametersQuery - errors', () => {
      it('should have a returnsAListOfTicketsBySearchParametersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsAListOfTicketsBySearchParametersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnectionsQuery - errors', () => {
      it('should have a retrieveExistingConnectionsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnectionsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnectionsQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnectionsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnectionsWithExtendedResourcesInformationQuery - errors', () => {
      it('should have a retrieveExistingConnectionsWithExtendedResourcesInformationQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnectionsWithExtendedResourcesInformationQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnectionsWithExtendedResourcesInformationQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnectionsWithExtendedResourcesInformationQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesApplicationIdentitiesQuery - errors', () => {
      it('should have a fetchesApplicationIdentitiesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesApplicationIdentitiesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingInterfaceConnectionsQuery - errors', () => {
      it('should have a retrieveExistingInterfaceConnectionsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingInterfaceConnectionsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingInterfaceConnectionsQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingInterfaceConnectionsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationInterfaceId', (done) => {
        try {
          a.retrieveExistingInterfaceConnectionsQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingInterfaceConnectionsQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingApplicationInterfacesQuery - errors', () => {
      it('should have a retrieveExistingApplicationInterfacesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingApplicationInterfacesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingApplicationInterfacesQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingApplicationInterfacesQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingConnectionsToApplicationQuery - errors', () => {
      it('should have a retrieveExistingConnectionsToApplicationQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingConnectionsToApplicationQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingConnectionsToApplicationQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingConnectionsToApplicationQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery - errors', () => {
      it('should have a retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveAListOfAllConnectionToApplicationPackByApplicationIDQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTagsFromApplicationPackQuery - errors', () => {
      it('should have a deleteTagsFromApplicationPackQuery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTagsFromApplicationPackQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deleteTagsFromApplicationPackQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteTagsFromApplicationPackQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.deleteTagsFromApplicationPackQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteTagsFromApplicationPackQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllApplicationPacksQuery - errors', () => {
      it('should have a listAllApplicationPacksQuery function', (done) => {
        try {
          assert.equal(true, typeof a.listAllApplicationPacksQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listAllApplicationPacksQuery(null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-listAllApplicationPacksQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeApplicationInterfacesFromApplicationPackQuery - errors', () => {
      it('should have a removeApplicationInterfacesFromApplicationPackQuery function', (done) => {
        try {
          assert.equal(true, typeof a.removeApplicationInterfacesFromApplicationPackQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.removeApplicationInterfacesFromApplicationPackQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-removeApplicationInterfacesFromApplicationPackQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationPackId', (done) => {
        try {
          a.removeApplicationInterfacesFromApplicationPackQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'applicationPackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-removeApplicationInterfacesFromApplicationPackQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServersQuery - errors', () => {
      it('should have a fetchesServersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingServersQuery - errors', () => {
      it('should have a retrieveExistingServersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingServersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingServersQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingServersQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServerQuery - errors', () => {
      it('should have a deleteServerQuery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServerQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteServerQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServerQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverId', (done) => {
        try {
          a.deleteServerQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServerQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesServicesQuery - errors', () => {
      it('should have a fetchesServicesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesServicesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnusedAndNonPredefinedGlobalServiceByNameQuery - errors', () => {
      it('should have a deleteUnusedAndNonPredefinedGlobalServiceByNameQuery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnusedAndNonPredefinedGlobalServiceByNameQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceQuery - errors', () => {
      it('should have a deleteServiceQuery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.deleteServiceQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServiceQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteServiceQuery('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteServiceQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveExistingServicesQuery - errors', () => {
      it('should have a retrieveExistingServicesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveExistingServicesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.retrieveExistingServicesQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-retrieveExistingServicesQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesUsersQuery - errors', () => {
      it('should have a fetchesUsersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesUsersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllChangesAudits - errors', () => {
      it('should have a getAllChangesAudits function', (done) => {
        try {
          assert.equal(true, typeof a.getAllChangesAudits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveCloudServerListingFromSecureTrackQuery - errors', () => {
      it('should have a retrieveCloudServerListingFromSecureTrackQuery function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveCloudServerListingFromSecureTrackQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateCloudServersFromSecureTrackWithAnApplicationQuery - errors', () => {
      it('should have a associateCloudServersFromSecureTrackWithAnApplicationQuery function', (done) => {
        try {
          assert.equal(true, typeof a.associateCloudServersFromSecureTrackWithAnApplicationQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.associateCloudServersFromSecureTrackWithAnApplicationQuery('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-associateCloudServersFromSecureTrackWithAnApplicationQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllCustomersQuery - errors', () => {
      it('should have a listAllCustomersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.listAllCustomersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllDomainsQuery - errors', () => {
      it('should have a listAllDomainsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.listAllDomainsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResources - errors', () => {
      it('should have a getResources function', (done) => {
        try {
          assert.equal(true, typeof a.getResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should have a getRole function', (done) => {
        try {
          assert.equal(true, typeof a.getRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getRole(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should have a getRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChangeQuery - errors', () => {
      it('should have a returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChangeQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheDetailsOfTheDevicesThatYouCanSelectAsATargetInSecureChangeQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatusQuery - errors', () => {
      it('should have a modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatusQuery function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTheSecureTrackConnectionSettingsAndTestTheConnectionStatusQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheListOfUsersQuery - errors', () => {
      it('should have a returnTheListOfUsersQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheListOfUsersQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getParty - errors', () => {
      it('should have a getParty function', (done) => {
        try {
          assert.equal(true, typeof a.getParty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getParty(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-getParty', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteParty - errors', () => {
      it('should have a deleteParty function', (done) => {
        try {
          assert.equal(true, typeof a.deleteParty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteParty(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-deleteParty', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveWorkflowsByType - errors', () => {
      it('should have a getActiveWorkflowsByType function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveWorkflowsByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchesVirtualServersFromSecureTrackQuery - errors', () => {
      it('should have a fetchesVirtualServersFromSecureTrackQuery function', (done) => {
        try {
          assert.equal(true, typeof a.fetchesVirtualServersFromSecureTrackQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChangeQuery - errors', () => {
      it('should have a returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChangeQuery function', (done) => {
        try {
          assert.equal(true, typeof a.returnsIDsForSpecifiedListOfExcludedParentManagementDevicesAndChildFirewallsInSecureChangeQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSuggestedTargetsForAGivenAccessRequestQuery - errors', () => {
      it('should have a getSuggestedTargetsForAGivenAccessRequestQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getSuggestedTargetsForAGivenAccessRequestQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importLdap - errors', () => {
      it('should have a importLdap function', (done) => {
        try {
          assert.equal(true, typeof a.importLdap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.importLdap(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-importLdap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeGroupMembers - errors', () => {
      it('should have a changeGroupMembers function', (done) => {
        try {
          assert.equal(true, typeof a.changeGroupMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.changeGroupMembers(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.changeGroupMembers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-changeGroupMembers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLocalGroup - errors', () => {
      it('should have a createLocalGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createLocalGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createLocalGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-createLocalGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePartyRoles - errors', () => {
      it('should have a updatePartyRoles function', (done) => {
        try {
          assert.equal(true, typeof a.updatePartyRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePartyRoles(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updatePartyRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePartyRoles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-updatePartyRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelTicket - errors', () => {
      it('should have a cancelTicket function', (done) => {
        try {
          assert.equal(true, typeof a.cancelTicket === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.cancelTicket(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-cancelTicket', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectATicketQuery - errors', () => {
      it('should have a rejectATicketQuery function', (done) => {
        try {
          assert.equal(true, typeof a.rejectATicketQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.rejectATicketQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-rejectATicketQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rejectATicketQuery('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-rejectATicketQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmTheRequestQuery - errors', () => {
      it('should have a confirmTheRequestQuery function', (done) => {
        try {
          assert.equal(true, typeof a.confirmTheRequestQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.confirmTheRequestQuery(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-confirmTheRequestQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.confirmTheRequestQuery('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-confirmTheRequestQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mapRules - errors', () => {
      it('should have a mapRules function', (done) => {
        try {
          assert.equal(true, typeof a.mapRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.mapRules(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-mapRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTriggers - errors', () => {
      it('should have a getTriggers function', (done) => {
        try {
          assert.equal(true, typeof a.getTriggers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTriggers - errors', () => {
      it('should have a addTriggers function', (done) => {
        try {
          assert.equal(true, typeof a.addTriggers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveServerQuery - errors', () => {
      it('should have a moveServerQuery function', (done) => {
        try {
          assert.equal(true, typeof a.moveServerQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.moveServerQuery(null, null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-tufin_securechange-adapter-moveServerQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
