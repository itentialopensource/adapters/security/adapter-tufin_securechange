# Tufin SecureChange

Vendor: Tufin
Homepage: https://www.tufin.com/

Product: SecureChange
Product Page: https://www.tufin.com/tufin-orchestration-suite/securechange

## Introduction
We classify Tufin SecureChange into the Security domain as Tufin SecureChange provides capabilities for defining, analyzing, provisioning, and enforcing security policies across firewalls, routers, and other network security devices.

"Tufin SecureChange provides policy-based automation and orchestration, enabling enterprises to implement accurate network changes in minutes instead of days"

## Why Integrate
The Tufin SecureChange adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Tufin SecureChange to provide comprehensive security policy management capabilities.

With this adapter you have the ability to perform operations with Tufin SecureChange such as:

- Retrieve, update, or create applications
- Customers
- Domains
- Retrieve, update, or submit a ticket

## Additional Product Documentation
The [API documents for Tufin SecureChange](https://forum.tufin.com/support/kc/rest-api/R24-1/securechangeworkflow/apidoc/)